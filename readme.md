# React Redux Seed
> This project is an application skeleton for a React with Redux app.

`React` is a UI library developed at Facebook to facilitate the creation of interactive, stateful & reusable UI components. It is used at Facebook in production, and Instagram.com is written entirely in React.

## Main Dependencies

| Name  | Version |
| ------------- | ------------- |
| react  | 0.14.5  |
| react-dom  |  0.14.3  |
| react-redux  |  4.0.6  |
| redux  |  3.0.5  |

## Usage

### Installation

```
$ sudo npm install
```

### Build App

Create <b>bundle.js</b> in public folder by the command:
```
$ npm run build
```

### Run App

Create <b>bundle.js</b> in public folder by the command:
```
$ npm run dev
```

Then. open <b>http://localhost:8080</b> file in any browser.

## Unit Testing in ReactJS
Unit tests are written following the [mocha](https://mochajs.org/) framework. All tests are located in `test/` and follow the naming convention `<test-name>.test.js`.

### Run Unit Testing
Tests are run using [Karma](http://karma-runner.github.io/). To run the full tests suite run:
```
$ npm test
```

Coverage reports can be generated with the following command
```
$ npm run coverage
```

The generated reports are located in `test/coverage/`

#### CI notes
To generate a coverage report in a continuous integration server, use the command:

```
$ npm run coverage-ci
```

This special coverage report task does not generate sourcemaps, making it more lightweight, and uses [PhantomJS](http://phantomjs.org/), a headless browser, to run the tests.

