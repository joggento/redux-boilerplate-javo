var _ = require('lodash');
var path = require('path');

/**
 * Base karma configuration.
 * The properties stated here will be common for every other configuration. This will serve as a
 * point of reuse for a local coverage configuration, and a different CI coverage configuration.
 * @type {Object}
 */
var baseConf = {
  plugins: [
    'karma-coverage',
    'karma-webpack',
    'karma-sourcemap-loader',
    'karma-chrome-launcher',
    'karma-phantomjs-launcher',
    'karma-mocha'
  ],

  // base path that will be used to resolve all patterns (eg. files, exclude)
  basePath: '../',

  // frameworks to use
  // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
  frameworks: ['mocha'],

  // list of files / patterns to load in the browser
  files: [
    'test/**/*.test.js'
  ],


  // list of files to exclude
  exclude: [
  ],

  // preprocess matching files before serving them to the browser
  preprocessors: {
    'test/**/*.test.js': ['webpack']
  },

  client: {
    captureConsole: true
  },

  webpack: {
    module: {
      preLoaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules)/,
          loader: 'babel',
          query: {
            presets: ['es2015', 'react'],
            cacheDirectory: true
          }
        }
      ]
    }
  },

  webpackMiddleware: {
    noInfo: true,
  },

  // test results reporter to use
  // possible values: 'dots', 'progress'
  // available reporters: https://npmjs.org/browse/keyword/karma-reporter
  reporters: ['progress'],

  // web server port
  port: 9876,

  // enable / disable colors in the output (reporters and logs)
  colors: true,

  // enable / disable watching file and executing tests whenever any file changes
  autoWatch: true,

  // start these browsers
  // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
  browsers: ['Chrome'],

  // Continuous Integration mode
  // if true, Karma captures browsers, runs the tests and exits
  singleRun: true,

  // Concurrency level
  // how many browser should be started simultaneous
  concurrency: Infinity
};

/**
 * Helper to provide lodash merge with deep array merging
 */
function customizer(objValue, srcValue) {
  if (_.isArray(objValue)) {
    return objValue.concat(srcValue);
  }
}

/**
 * Base Karma configuration for running a coverage analysis.
 * Based on the base config, this one is an extension to add all data is needed for a coverage
 * instrumentation. It is also a base config, as it will be reused in a local and a CI coverage task.
 * @type {Object}
 */
var coverageConf = _.merge({}, baseConf, {
  webpack: {
    isparta: {
      embedSource: true,
      noAutoWrap: true,
      print: 'detail',
      babel: {
        presets: ['es2015', 'react']
      }
    },
    module: {
      preLoaders: [
        {
          test: /\.js$/,
          include: path.resolve('src'),
          loader: 'isparta-loader'
        }
      ]
    }
  },

  coverageReporter : {
    type: 'lcov',
    dir: 'test/coverage/'
  },

  reporters: ['coverage']
}, customizer);

module.exports.baseConf = baseConf;
module.exports.coverageConf = coverageConf;