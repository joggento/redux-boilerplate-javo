var coverageConf = require('./karma.base').coverageConf;

module.exports = function(config) {
  coverageConf.logLevel = config.LOG_INFO;
  config.set(coverageConf);
}
