require('./main.scss');

var React = require('react');
var Component = require('react').Component;
var PropTypes = require('react').PropTypes;

class Main extends Component {
  render(){
    return (
      <div className="main">
        <div className="title">Hello, you are in main page!!</div>
      </div>
    )
  }
}

Main.propTypes = {
}

module.exports = Main;
