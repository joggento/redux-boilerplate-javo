require('./header.scss');

var React = require('react');
var Component = require('react').Component;
var PropTypes = require('react').PropTypes;
var Link = require('react-router').Link;
var connect = require('react-redux').connect;
var routeActions= require('redux-simple-router').routeActions;

function Header({ push, children }) {
  return (
    <div className="header">
      <ul>
        Links:
        {' '}
        <li><Link to="/home">Home</Link></li>
        {' '}
        <li><Link to="/counter">Counter</Link></li>
        {' '}
        <li><Link to="/foo">Foo</Link></li>
      </ul>
    </div>
  );
};

module.exports = connect(
  null,
  { push: routeActions.push }
)(Header);
