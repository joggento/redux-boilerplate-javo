var React = require('react');
var Component = require('react').Component;
var PropTypes = require('react').PropTypes;
var Header = require('../header/header');
const DevTools = require('../../utilities/devTools');

class App extends Component {
  render(){
    return (
          <div>
            <Header></Header>
            <div style={{marginTop: '1.5em'}}>{this.props.children}</div>
            <DevTools />
          </div>
    );
  }
}

App.propTypes = {
}

module.exports = App;
