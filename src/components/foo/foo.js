require('./foo.scss');

var React = require('react');
var Component = require('react').Component;
var PropTypes = require('react').PropTypes;

class Foo extends Component {
  render(){
    return (
      <div className="foo">
        <div className="title">Hello, you are in Foo page!! =)</div>
      </div>
    )
  }
}

Foo.propTypes = {
}

module.exports = Foo;
