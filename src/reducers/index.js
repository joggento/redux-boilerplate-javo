var combineReducers = require('redux').combineReducers;
var counter = require('./counter');
var routeReducer = require('redux-simple-router').routeReducer;

const rootReducer = combineReducers({
  counter,
  routing: routeReducer
});

module.exports = rootReducer;
