const Redux = require('redux');
const thunk = require('redux-thunk');
const reducers = require('../reducers');
const DevTools = require('../utilities/devTools');
const { syncHistory } = require('redux-simple-router');

function configureStore(history) {

  const reduxRouterMiddleware  = syncHistory(history);

  const createStoreWithMiddleware = Redux.applyMiddleware(
    thunk, reduxRouterMiddleware
  )(Redux.createStore);

  const finalCreateStore = Redux.compose(
    // Middleware you want to use in development:

    Redux.applyMiddleware(thunk, reduxRouterMiddleware),
    // Required! Enable Redux DevTools with the monitors you chose
    DevTools.instrument()
  )(Redux.createStore);

  // IF DEV
  const store = finalCreateStore(reducers);

  // IF PROD
  //const store = createStoreWithMiddleware(reducers, initialState);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers');
      store.replaceReducer(nextReducer);
    });
  }

  reduxRouterMiddleware.listenForReplays(store);

  return store;
}

module.exports = configureStore;
