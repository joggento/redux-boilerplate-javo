var React = require('react');
var ReactDOM = require('react-dom');
var Redux = require('redux');
var thunk = require('redux-thunk');
var Provider = require('react-redux').Provider;
var Router = require('react-router').Router;
const { createHistory } = require('history');
var configureStore = require('./store/configureStore');

var getRoutes = require('./routes/');

const history = createHistory();

const store = configureStore(history);

ReactDOM.render(
  <Provider store={store}>
      <Router history={history}>
             { getRoutes() }
      </Router>
  </Provider>,
  document.getElementById('app')
)
