require('./style.scss');
var React = require('react');
var Header=require('../../components/header/header.js');
var Foo=require('../../components/foo/foo.js');

var foo = React.createClass({
  render: function(){
    return(
      <div className="foo-page">
        <Foo></Foo>
      </div>
    );
  }
});

module.exports=foo;
