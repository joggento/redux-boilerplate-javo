require('./style.scss');
var React = require('react');
var Main=require('../../components/main/main');

var home = React.createClass({
  render: function(){
    return(
      <div className="home-page">
        <Main></Main>
      </div>
    );
  }
});

module.exports=home;
