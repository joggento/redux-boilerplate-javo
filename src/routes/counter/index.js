require('./style.scss');
var React = require('react');
var Counter=require('../../components/counter/counter.js');

var counter = React.createClass({
  render: function(){
    return(
      <div className="counter-page">
        <Counter></Counter>
      </div>
    );
  }
});

module.exports=counter;
