var React = require('react');
var Route = require('react-router').Route;
var HomePage = require('./home/');
var CounterPage = require('./counter/');
var FooPage = require('./foo/');
var AppComponent = require('../components/app/app');

var getRoutes=function(){
  return (
          <Route path="/" component={AppComponent} >
            <Route path="home" component={HomePage} />
            <Route path="foo" component={FooPage} />
            <Route path="counter" component={CounterPage} />
          </Route>
    )};

module.exports=getRoutes;
