'use strict';

var request = require('superagent');
var Promise = require('es6-promise').Promise;

/**
 * Wrapper for calling a API
 */
var Api = {
  get: function (url) {
    console.log('GET API Service');
    return new Promise(function (resolve, reject) {
      request
        .get(url)
        .set('Accept', 'application/json')
        .end(function (err,res) {
          if (res!==null){
            if (res.status === 404) {
              reject();
            } else {
              console.log('response: '+res.body);
              resolve(JSON.parse(res.body));
            }
          }
          //TODO: For the moment for testing
          else {
            resolve('res');
            //reject();
          }
        });
    });
  }
};

module.exports = Api;
